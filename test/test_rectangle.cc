#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include "core/voot_binding.hh"
#include "gui/voot_rectangle.hh"
#include "voot_rectangle.cc"

#include "gui/voot_window.hh"

struct Test
{
    void slot(voot::MouseButton button, int x, int y) noexcept
    {
        VT_LOG_INFO("RootItem mouse button pressed B: {} X: {} Y: {}", button, x, y);
    }
};

void free_function(voot::MouseButton button, int x, int y) noexcept
{
    VT_LOG_INFO("Blue Rectangle mouse button pressed B: {} X: {} Y: {}", button, x, y);
}

TEST_CASE("Rectangle", "[rectangle]")
{
    using namespace voot;

    Application app;
    voot::Window window{ "Rectangles" };

    Test t;

    window.root_item()->mouse_button_pressed.connect<&Test::slot>(t);
    auto *r = new voot::Rectangle();
    r->mouse_button_pressed.connect<&free_function>();

    auto *r2 = new voot::Rectangle();
    r2->mouse_button_pressed.connect([](MouseButton button, int x, int y) {
        VT_LOG_INFO("Red Rectangle mouse button pressed B: {} X: {} Y: {}", button, x, y);
    });

    auto *r3 = new voot::Rectangle();
    r3->mouse_clicked.connect([](MouseButton button, int x, int y) {
        VT_LOG_INFO("Green Rectangle mouse clicked B: {} X: {} Y: {}", button, x, y);
    });
    r3->mouse_double_clicked.connect([](MouseButton button, int x, int y) {
        VT_LOG_INFO("Green Rectangle mouse double clicked B: {} X: {} Y: {}", button, x, y);
    });

    // r->x = window.root_item()->width() - 140;
    voot::bind(r->x, [&window, &r]() -> int {
        return window.root_item()->width() - r->width() + 100;
    });
    r->y = 20;
    r->width = 500;
    r->height = 200;
    r->color = { 0, 0, 255, 255 };

    r2->x = 20;
    r2->y = 20;
    r2->z = 1;
    r2->width = 300;
    r2->height = 150;
    r2->color = { 255, 0, 0, 255 };

    r3->x = 40;
    r3->y = 40;
    r3->set_z(4);
    r3->width = 300;
    r3->height = 150;
    r3->color = { 0, 255, 0, 255 };

    r->set_parent_item(window.root_item());
    r2->set_parent_item(r);
    r3->set_parent_item(r);

    app.exec();
}
