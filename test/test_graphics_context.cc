#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include "core/voot_graphics_context.hh"
#include "voot_graphics_context_gles.cc"
#include "voot_graphics_context_vulkan.cc"

TEST_CASE("GraphicsContext", "[]")
{}
