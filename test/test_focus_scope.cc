#include <catch2/catch.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wkeyword-macro"
#endif
#define private public
#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include "gui/voot_focus_scope.hh"
#include "voot_focus_scope.cc"

#include "core/voot_application.hh"
#include "core/voot_binding.hh"

#include "gui/voot_rectangle.hh"
#include "gui/voot_window.hh"

TEST_CASE("Test focus scope", "[focus_scope]")
{
    using namespace voot;

    Application app;
    voot::Window window{ "Focus Scope" };

    auto *r = new voot::Rectangle();
    auto *r2 = new voot::Rectangle();
    auto *r3 = new voot::Rectangle();

    // r->x = window.root_item()->width() - 140;
    voot::bind(r->x, [&window, &r]() -> int {
        return window.root_item()->width() - r->width() + 100;
    });
    r->y = 20;
    r->width = 500;
    r->height = 200;

    voot::bind(r->color, [r]() -> Rectangle::Color {
        if (r->focus())
        {
            return { 255, 255, 255, 255 };
        }
        return { 0, 0, 255, 255 };
    });

    r2->x = 20;
    r2->y = 20;
    r2->z = 1;
    r2->width = 300;
    r2->height = 150;
    r2->color = { 255, 0, 0, 255 };

    r3->x = 40;
    r3->y = 40;
    r3->set_z(4);
    r3->width = 300;
    r3->height = 150;
    r3->color = { 0, 255, 0, 255 };

    r->set_parent_item(window.root_item());
    r2->set_parent_item(r);
    r3->set_parent_item(r);

    //    if (!focus())
    //    {
    //        paint = SkPaint(SkColor4f{ float(c.red) / 255,
    //            float(c.green) / 255,
    //            float(c.blue) / 255,
    //            float(c.alpha) / 255 });
    //    }
    //    else
    //    {
    //        paint = SkPaint(SkColor4f{ 1.F, 1.F, 1.F, 1.F });
    //    }

    app.exec();
}
