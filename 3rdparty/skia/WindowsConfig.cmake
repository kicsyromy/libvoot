if (WIN32)
    set (VOOT_SKIA_LIB_PREFIX "")
    set (VOOT_SKIA_LIB_SUFFIX ".lib")

    if (CMAKE_BUILD_TYPE STREQUAL "Debug")
        set (
            SKIA_C_FLAGS
            ${SKIA_C_FLAGS}
            "/MDd"
        )

        set (
            SKIA_CXX_FLAGS
            ${SKIA_CXX_FLAGS}
            "/MDd"
        )
    else ()
        set (
            SKIA_C_FLAGS
            ${SKIA_C_FLAGS}
            "/MD"
        )

        set (
            SKIA_CXX_FLAGS
            ${SKIA_CXX_FLAGS}
            "/MD"
        )
    endif ()
endif ()
