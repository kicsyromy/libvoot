if (VOOT_SKIA_GRAPHICS_BACKEND STREQUAL "Vulkan")
    set (VOOT_GRAPHICS_BACKEND ${VOOT_SKIA_GRAPHICS_BACKEND})

    find_package (Vulkan)

    target_compile_definitions (
        skia INTERFACE
        -DSK_VULKAN
        -DVOOT_BACKEND_VULKAN
    )

    set (
        SKIA_GN_ARGS
        ${SKIA_GN_ARGS}
        skia_use_gl=false
        skia_use_vulkan=true
    )

    target_link_libraries (
        skia INTERFACE
        Vulkan::Vulkan
    )
endif ()