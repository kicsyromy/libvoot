if (UNIX AND NOT APPLE)
    find_package (Fontconfig REQUIRED)
    find_package (Freetype REQUIRED)

    set (VOOT_SKIA_LIB_PREFIX "lib")
    set (VOOT_SKIA_LIB_SUFFIX ".a")

    target_link_libraries (
        skia INTERFACE
        Fontconfig::Fontconfig
        Freetype::Freetype
    )
endif ()
