if (VOOT_SKIA_GRAPHICS_BACKEND STREQUAL "OpenGLES")
    set (VOOT_GRAPHICS_BACKEND ${VOOT_SKIA_GRAPHICS_BACKEND})

    find_package (PkgConfig QUIET REQUIRED)
    pkg_search_module (GLESv2 REQUIRED IMPORTED_TARGET glesv2)
    pkg_search_module (EGL REQUIRED IMPORTED_TARGET egl)

    target_compile_definitions (
        skia INTERFACE
        -DSK_GL
        -DVOOT_BACKEND_GLES
    )

    set (
        SKIA_GN_ARGS
        ${SKIA_GN_ARGS}
        skia_use_egl=true
        skia_use_gles=true
    )

    target_link_libraries (
        skia INTERFACE
        PkgConfig::GLESv2
        PkgConfig::EGL
    )
endif ()


