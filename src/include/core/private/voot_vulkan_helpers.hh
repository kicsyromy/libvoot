#ifdef VOOT_BACKEND_VULKAN

#pragma once

#include "voot_global.hh"

#include <vulkan/vulkan.h>

#include <cstdint>
#include <memory>
#include <utility>
#include <vector>

using SDL_Window = struct SDL_Window;

class GrBackendRenderTarget;
class GrContext;
class SkSurface;

VT_BEGIN_NAMESPACE

namespace vulkan
{
    struct PhysicalDevice
    {
        VkPhysicalDevice device;
        std::uint32_t queue_family_index;
    };

    struct Device
    {
        VkDevice device;
        std::uint32_t queue_family_index;
    };

    std::pair<std::vector<const char *>, VkResult> available_layers();
    std::pair<std::vector<const char *>, VkResult> available_extensions();

    std::pair<VkInstance, VkResult> create_instance(std::uint32_t api_version,
        const char *const *required_layers,
        std::size_t layers_count,
        const char *const *required_extensions,
        std::size_t extension_count);

    namespace device
    {
        std::pair<std::vector<PhysicalDevice>, VkResult> available_physical_devices(
            VkInstance vk_instance,
            VkQueueFlags vk_queue_flag);

        std::pair<Device, VkResult> create_logical_device(PhysicalDevice physical_device,
            const char *const *required_layers,
            std::size_t layers_count,
            const char *const *required_extensions,
            std::size_t extension_count);

    } // namespace device

    namespace swap_chain
    {
        std::pair<VkSwapchainKHR, VkResult> create(SDL_Window *window,
            VkInstance instance,
            PhysicalDevice physical_device,
            Device logical_device,
            VkSurfaceTransformFlagBitsKHR surface_transform,
            VkFormat surface_format,
            VkColorSpaceKHR color_space,
            VkPresentModeKHR present_mode,
            const VkImageUsageFlags *image_usage_flags,
            std::size_t image_usage_flag_count,
            VkSwapchainKHR retired_swap_chain = nullptr);
        std::pair<std::vector<VkImage>, VkResult> image_handles(VkSwapchainKHR swap_chain,
            VkDevice logical_device);

    } // namespace swap_chain

    namespace queue
    {
        VkQueue create(Device device);
        VkResult present(VkQueue device_queue,
            const VkSwapchainKHR *swap_chains,
            std::size_t swap_chain_count,
            const std::uint32_t *image_indices);
    } // namespace queue

    namespace skia
    {
        std::unique_ptr<GrContext> create_context(VkInstance instance,
            std::uint32_t instance_api_version,
            PhysicalDevice physical_device,
            Device logical_device,
            VkQueue device_queue,
            const char *const *instance_extensions,
            std::size_t instance_extension_count,
            const char *const *device_extensions,
            std::size_t device_extension_count);

        std::pair<std::unique_ptr<SkSurface>, std::unique_ptr<GrBackendRenderTarget>>
        create_surface(SDL_Window *window,
            GrContext &skia_context,
            VkImage image_handle,
            VkFormat swap_chain_format,
            PhysicalDevice physical_device);
    } // namespace skia
} // namespace vulkan

VT_END_NAMESPACE

#endif