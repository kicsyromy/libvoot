#pragma once

#include "voot_global.hh"

#include <memory>

class GrDirectContext;
class SkSurface;

using SDL_Window = struct SDL_Window;

VT_BEGIN_NAMESPACE

class VOOT_API GraphicsContext
{
public:
    class VOOT_API Surface
    {
    public:
        Surface(const GraphicsContext &context, SDL_Window *owning_window);
        ~Surface() noexcept;

    public:
        void set_as_current();
        void refresh();
        void present();

        SkSurface &surface_handle() noexcept
        {
            return *surface_handle_;
        }

    private:
#ifdef _MSC_VER
#pragma warning(disable : 4251)
#endif
        std::unique_ptr<SkSurface, void (*)(SkSurface *)> surface_handle_;
        std::unique_ptr<void, void (*)(void *)> surface_data_;
#ifdef _MSC_VER
#pragma warning(default : 4251)
#endif
        const GraphicsContext &graphics_context_;
        SDL_Window *owning_window_;

    private:
        VOOT_DISABLE_COPY(Surface);
        VOOT_DISABLE_MOVE(Surface);

    private:
        friend class ::voot::GraphicsContext;
    };

public:
    GraphicsContext() noexcept;
    ~GraphicsContext() noexcept;

private:
#ifdef _MSC_VER
#pragma warning(disable : 4251)
#endif
    std::unique_ptr<void, void (*)(void *)> native_context_{ nullptr, nullptr };
#ifdef _MSC_VER
#pragma warning(default : 4251)
#endif

private:
    VOOT_DISABLE_COPY(GraphicsContext);
    VOOT_DISABLE_MOVE(GraphicsContext);

private:
    friend class ::voot::GraphicsContext::Surface;
};

VT_END_NAMESPACE