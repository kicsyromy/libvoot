#pragma once

#include "gui/voot_item.hh"
#include "voot_global.hh"

VT_BEGIN_NAMESPACE

class FocusScope : public Item
{
public:
    enum class FocusActionResult
    {
        ChildFocused,
        CannotFocus
    };

public:
    FocusScope()
      : Item()
    {
        mouse_event_filter_ = MouseEventFilterNone;
        is_focus_scope_ = true;
        focus.changed.connect<&FocusScope::on_focus_changed>(*this);
    }

public:
    void move_next();
    void move_previous();

    constexpr Item *focused_child() const noexcept
    {
        return focused_child_;
    }

    inline void render(SkCanvas *canvas) const noexcept
    {
        static_cast<const Item *>(this)->render(canvas);
    }

private:
    void on_focus_changed(bool focus);
    FocusActionResult update_focus();

private:
    Item *focused_child_{ nullptr };
    int focused_child_z_{ 0 };
};

VT_END_NAMESPACE
