#pragma once

#include "voot_global.hh"

#include "core/voot_property.hh"

#include "gui/voot_item.hh"

VT_BEGIN_NAMESPACE

class Rectangle : public ItemBase<Rectangle>
{
    VT_ITEM()

public:
    struct Color
    {
        std::uint8_t red;
        std::uint8_t green;
        std::uint8_t blue;
        std::uint8_t alpha;
    };

public:
    Rectangle()
    {
        set_mouse_event_filter(MouseEventFilterButton);
    }

public:
    constexpr Color get_color() const noexcept
    {
        return color_;
    }

    constexpr bool set_color(const Color &c) noexcept
    {
        if (color_.red != c.red || color_.green != c.green || color_.blue != c.blue ||
            color_.alpha != c.alpha)
        {
            color_ = c;
            return true;
        }

        return false;
    }

public:
    VT_PROPERTY(Color, color, &Rectangle::get_color, &Rectangle::set_color);

public:
    void render(SkCanvas *canvas) const noexcept;

private:
    Color color_;
};

VT_END_NAMESPACE
