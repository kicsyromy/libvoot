#ifdef VOOT_BACKEND_GLES

#include "core/voot_graphics_context.hh"
#include "core/voot_logger.hh"

#include <SDL2.h>
#include <SDL_egl.h>
#include <SDL_opengles2.h>

#include <gsl/gsl>

#ifdef _MSC_VER
#pragma warning(disable : 4244 4201 4242 4996)
#endif
#include <core/SkCanvas.h>
#include <core/SkSurface.h>
#include <gpu/GrDirectContext.h>
#include <gpu/gl/GrGLInterface.h>
#include <src/gpu/gl/GrGLDefines.h>
#ifdef _MSC_VER
#pragma warning(default : 4244 4201 4242 4996)
#endif

namespace
{
    struct OpenGLESGraphicsContext
    {
        std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> window{ nullptr, nullptr };
        gsl::owner<void *> gl_context{ nullptr };
        std::unique_ptr<GrContext, void (*)(GrContext *)> skia_context{ nullptr, nullptr };
        sk_sp<const GrGLInterface> gl_interface{ nullptr };
    };
} // namespace

VT_BEGIN_NAMESPACE

GraphicsContext::Surface::Surface(const GraphicsContext &context, SDL_Window *owning_window)
  : surface_handle_{ nullptr,
      [](SkSurface *self) {
          if (self != nullptr)
          {
              self->unref();
          }
      } }
  , surface_data_{ nullptr,
      [](void *render_target) {
          if (render_target != nullptr)
          {
              auto *self = static_cast<GrBackendRenderTarget *>(render_target);
              delete self;
          }
      } }
  , graphics_context_{ context }
  , owning_window_{ owning_window }
{
    refresh();
}

GraphicsContext::Surface::~Surface() noexcept = default;

void GraphicsContext::Surface::set_as_current()
{
    const auto *gl_graphics_context =
        static_cast<const OpenGLESGraphicsContext *>(graphics_context_.native_context_.get());
    SDL_GL_MakeCurrent(owning_window_, gl_graphics_context->gl_context);
}

void GraphicsContext::Surface::refresh()
{
    const auto *gl_graphics_context =
        static_cast<const OpenGLESGraphicsContext *>(graphics_context_.native_context_.get());

    std::int32_t content_width{};
    std::int32_t content_height{};
    SDL_GL_GetDrawableSize(owning_window_, &content_width, &content_height);

    auto &interface = gl_graphics_context->gl_interface;

    GrGLint buffer;
    interface->fFunctions.fGetIntegerv(GL_FRAMEBUFFER_BINDING, &buffer);
    GrGLFramebufferInfo info;
    info.fFBOID = GrGLuint(buffer);
    SkColorType color_type = kBGRA_8888_SkColorType;
    info.fFormat = GR_GL_BGRA8;

    SkSurfaceProps surface_properties(SkSurfaceProps::kLegacyFontHost_InitType);
    auto *render_target = new GrBackendRenderTarget(content_width, content_height, 0, 8, info);
    sk_sp<SkSurface> surface(
        SkSurface::MakeFromBackendRenderTarget(gl_graphics_context->skia_context.get(),
            *render_target,
            kBottomLeft_GrSurfaceOrigin,
            color_type,
            nullptr,
            &surface_properties));

    if (surface == nullptr)
    {
        VT_LOG_FATAL("Failed to create Skia render surface");
    }

    surface_handle_.reset(surface.release());
    surface_data_.reset(render_target);
}

void GraphicsContext::Surface::present()
{
    SDL_GL_SwapWindow(owning_window_);
}

GraphicsContext::GraphicsContext() noexcept
{
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 0);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        VT_LOG_FATAL("Failed to initialize video subsystem: {}", SDL_GetError());
    }

    auto *gl_graphics_context = new OpenGLESGraphicsContext();
    gl_graphics_context->window = std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>{
        SDL_CreateWindow("",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            0,
            0,
            SDL_WINDOW_OPENGL | SDL_WINDOW_HIDDEN),
        &SDL_DestroyWindow
    };
    gl_graphics_context->gl_context = SDL_GL_CreateContext(gl_graphics_context->window.get());

    auto skia_context = GrDirectContext::MakeGL();
    if (skia_context == nullptr)
    {
        VT_LOG_FATAL("Failed to create Skia GL context");
    }

    gl_graphics_context->skia_context = decltype(
        OpenGLESGraphicsContext::skia_context){ skia_context.release(), [](GrContext *self) {
                                                   self->releaseResourcesAndAbandonContext();
                                                   self->unref();
                                               } };
    gl_graphics_context->gl_interface = GrGLMakeNativeInterface();

    native_context_ =
        decltype(native_context_){ gl_graphics_context, [](void *context) {
                                      auto *self = static_cast<OpenGLESGraphicsContext *>(context);
                                      delete self;
                                  } };
}

GraphicsContext::~GraphicsContext() noexcept = default;

VT_END_NAMESPACE

#endif