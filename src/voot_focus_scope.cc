#include "gui/voot_focus_scope.hh"

VT_BEGIN_NAMESPACE

void FocusScope::move_next()
{
    if (focus())
    {
        update_focus();
    }
}

void FocusScope::move_previous()
{}

void FocusScope::on_focus_changed(bool set_focus)
{
    if (!set_focus && focused_child_ != nullptr)
    {
        focused_child_->focus = false;
    }
    else
    {
        update_focus();
    }
}

FocusScope::FocusActionResult FocusScope::update_focus()
{
    if (children_.empty())
    {
        return FocusActionResult::CannotFocus;
    }

    Item *previously_focused_child{ nullptr };
    int start_z{ z_max_ };
    if (focused_child_ != nullptr)
    {
        if (focused_child_->is_focus_scope_)
        {
            const auto focus_result = static_cast<FocusScope *>(focused_child_)->update_focus();

            if (focus_result == FocusActionResult::CannotFocus)
            {
                previously_focused_child = focused_child_;
                focused_child_ = nullptr;
                previously_focused_child->focus = false;
                start_z = focused_child_z_;
            }
            else
            {
                return FocusActionResult::ChildFocused;
            }
        }
        else
        {
            previously_focused_child = focused_child_;
            focused_child_ = nullptr;
            previously_focused_child->focus = false;
            start_z = focused_child_z_;
        }
    }

    bool child_focused = false;

    for (int current_z = start_z; current_z >= z_min_; --current_z)
    {
        auto children_at_current_z_it = children_.find(current_z);
        if (children_at_current_z_it == children_.end())
        {
            continue;
        }

        const auto &children_at_current_z = children_at_current_z_it->second;

        for (auto &child : children_at_current_z)
        {
            if (!child->can_focus() || child.get() == previously_focused_child)
            {
                continue;
            }

            if (child->is_focus_scope_)
            {
                const auto focus_result = static_cast<FocusScope *>(child.get())->update_focus();
                if (focus_result == FocusActionResult::ChildFocused)
                {
                    focused_child_ = child.get();
                    focused_child_z_ = current_z;
                    child_focused = true;

                    break;
                }
            }
            else
            {
                focused_child_ = child.get();
                focused_child_->focus = true;
                focused_child_z_ = current_z;
                child_focused = true;

                break;
            }
        }

        if (child_focused)
        {
            break;
        }
    }

    if (!child_focused)
    {
        return FocusActionResult::CannotFocus;
    }
    else
    {
        return FocusActionResult::ChildFocused;
    }
}

VT_END_NAMESPACE
