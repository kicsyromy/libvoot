#ifdef VOOT_BACKEND_VULKAN

#include "core/private/voot_vulkan_helpers.hh"
#include "core/voot_graphics_context.hh"
#include "core/voot_logger.hh"

#include <SDL2.h>
#include <SDL_vulkan.h>

#ifdef _MSC_VER
#pragma warning(disable : 4244 4201 4242 4996)
#endif
#include <core/SkCanvas.h>
#include <core/SkSurface.h>
#include <gpu/GrContext.h>
#include <gpu/vk/GrVkBackendContext.h>
#include <gpu/vk/GrVkExtensions.h>
#ifdef _MSC_VER
#pragma warning(default : 4244 4201 4242 4996)
#endif

#include <array>
#include <cstdint>
#include <string_view>
#include <vector>

namespace
{
    constexpr std::uint32_t VK_VERSION{ VK_API_VERSION_1_0 };
    constexpr std::array INSTANCE_LAYERS{ "VK_LAYER_LUNARG_standard_validation" };
    constexpr auto &DEVICE_LAYERS = INSTANCE_LAYERS;

    constexpr std::array DEVICE_EXTENSIONS{ VK_KHR_SWAPCHAIN_EXTENSION_NAME };
    constexpr VkQueueFlags DEVICE_QUEUE_FLAGS{ VK_QUEUE_GRAPHICS_BIT };

    constexpr VkPresentModeKHR PRESENTATION_MODE{ VK_PRESENT_MODE_FIFO_KHR };
    constexpr VkImageUsageFlags IMAGE_USAGE{ VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT };
    constexpr VkSurfaceTransformFlagBitsKHR SURFACE_TRANSFORMS{
        VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR
    };
    constexpr VkFormat SURFACE_FORMAT{ VK_FORMAT_B8G8R8A8_UNORM };
    constexpr VkColorSpaceKHR COLOR_SPACE{ VK_COLORSPACE_SRGB_NONLINEAR_KHR };

    struct VulkanGraphicsContext
    {
        std::unique_ptr<VkInstance_T, void (*)(VkInstance)> vk_instance{ nullptr, nullptr };
        voot::vulkan::PhysicalDevice physical_device{ nullptr, 0 };
        std::unique_ptr<VkDevice_T, void (*)(VkDevice)> logical_device{ nullptr, nullptr };
        VkQueue device_queue{ nullptr };
        std::unique_ptr<GrContext, void (*)(GrContext *)> skia_context{ nullptr, nullptr };
    };

    struct SkiaSurface
    {
        VkQueue device_queue{ nullptr };
        VkSwapchainKHR swap_chain{ nullptr };
        std::vector<VkImage> image_handles{};
        std::unique_ptr<GrBackendRenderTarget> render_target{ nullptr };
    };

} // namespace

VT_BEGIN_NAMESPACE

GraphicsContext::Surface::Surface(const GraphicsContext &context, SDL_Window *owning_window)
  : surface_handle_{ nullptr,
      [](SkSurface *self) {
          if (self != nullptr)
          {
              self->unref();
          }
      } }
  , surface_data_{ new SkiaSurface,
      [](void *surface) {
          auto *self = static_cast<SkiaSurface *>(surface);
          delete self;
      } }
  , graphics_context_{ context }
  , owning_window_{ owning_window }
{
    const auto *vk_graphics_context =
        static_cast<const VulkanGraphicsContext *>(context.native_context_.get());
    auto *skia_surface = static_cast<SkiaSurface *>(surface_data_.get());
    skia_surface->device_queue = vk_graphics_context->device_queue;

    refresh();
}

GraphicsContext::Surface::~Surface() noexcept = default;

void GraphicsContext::Surface::set_as_current()
{}

void GraphicsContext::Surface::refresh()
{
    const auto *vk_graphics_context =
        static_cast<const VulkanGraphicsContext *>(graphics_context_.native_context_.get());
    auto *skia_surface = static_cast<SkiaSurface *>(surface_data_.get());

    surface_handle_.reset();
    skia_surface->render_target.reset();

    auto swap_chain = vulkan::swap_chain::create(owning_window_,
        vk_graphics_context->vk_instance.get(),
        vk_graphics_context->physical_device,
        { vk_graphics_context->logical_device.get(),
            vk_graphics_context->physical_device.queue_family_index },
        SURFACE_TRANSFORMS,
        SURFACE_FORMAT,
        COLOR_SPACE,
        PRESENTATION_MODE,
        &IMAGE_USAGE,
        1,
        skia_surface->swap_chain);
    if (swap_chain.second != VK_SUCCESS)
    {
        VT_LOG_FATAL("Failed to create Vulkan swap chain");
    }

    skia_surface->swap_chain = swap_chain.first;

    auto image_handles = vulkan::swap_chain::image_handles(swap_chain.first,
        vk_graphics_context->logical_device.get());
    if (image_handles.second != VK_SUCCESS)
    {
        VT_LOG_FATAL("Failed to get swap chain image handles");
    }

    skia_surface->image_handles = std::move(image_handles.first);

    auto skia_surface_data = vulkan::skia::create_surface(owning_window_,
        *(vk_graphics_context->skia_context),
        skia_surface->image_handles[0],
        SURFACE_FORMAT,
        vk_graphics_context->physical_device);

    surface_handle_.reset(skia_surface_data.first.release());
    skia_surface->render_target = std::move(skia_surface_data.second);
}

void GraphicsContext::Surface::present()
{
    auto *skia_surface = static_cast<SkiaSurface *>(surface_data_.get());
    const std::uint32_t image_indices[]{ 0 };
    vulkan::queue::present(skia_surface->device_queue,
        &skia_surface->swap_chain,
        1,
        static_cast<const std::uint32_t *>(image_indices));
}

GraphicsContext::GraphicsContext() noexcept
{
    auto window = std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)>{
        SDL_CreateWindow("",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            0,
            0,
            SDL_WINDOW_VULKAN | SDL_WINDOW_HIDDEN),
        &SDL_DestroyWindow
    };

    std::uint32_t extension_count = 0;
    if (SDL_Vulkan_GetInstanceExtensions(window.get(), &extension_count, nullptr) != SDL_TRUE)
    {
        VT_LOG_FATAL("Failed to query required Vulkan extensions: {}", SDL_GetError());
    }

    std::vector<const char *> required_instance_extensions;
    required_instance_extensions.resize(extension_count);
    if (SDL_Vulkan_GetInstanceExtensions(window.get(),
            &extension_count,
            required_instance_extensions.data()) != SDL_TRUE)
    {
        VT_LOG_FATAL("Failed to query required Vulkan extensions: {}", SDL_GetError());
    }

    auto vk_instance = vulkan::create_instance(VK_VERSION,
        INSTANCE_LAYERS.data(),
        INSTANCE_LAYERS.size(),
        required_instance_extensions.data(),
        required_instance_extensions.size());
    if (vk_instance.second != VK_SUCCESS)
    {
        VT_LOG_FATAL("Failed to create Vulkan instance");
    }

    auto available_devices =
        vulkan::device::available_physical_devices(vk_instance.first, DEVICE_QUEUE_FLAGS);
    if (available_devices.second != VK_SUCCESS)
    {
        VT_LOG_FATAL("Failed to get available GPUs");
    }

    // TODO: Figure out a smarter way to choose a device
    auto &physical_device = available_devices.first[0];

    auto logical_device = vulkan::device::create_logical_device(physical_device,
        DEVICE_LAYERS.data(),
        DEVICE_LAYERS.size(),
        DEVICE_EXTENSIONS.data(),
        DEVICE_EXTENSIONS.size());
    if (logical_device.second != VK_SUCCESS)
    {
        VT_LOG_FATAL("Failed to create logical device");
    }

    auto *device_queue = vulkan::queue::create(logical_device.first);

    auto skia_context = vulkan::skia::create_context(vk_instance.first,
        VK_VERSION,
        physical_device,
        logical_device.first,
        device_queue,
        required_instance_extensions.data(),
        required_instance_extensions.size(),
        DEVICE_EXTENSIONS.data(),
        DEVICE_EXTENSIONS.size());

    auto *vk_graphics_context = new VulkanGraphicsContext();
    vk_graphics_context->vk_instance =
        decltype(VulkanGraphicsContext::vk_instance){ vk_instance.first, [](VkInstance self) {
                                                         vkDestroyInstance(self, nullptr);
                                                     } };
    vk_graphics_context->physical_device = physical_device;
    vk_graphics_context->logical_device = decltype(
        VulkanGraphicsContext::logical_device){ logical_device.first.device, [](VkDevice self) {
                                                   vkDestroyDevice(self, nullptr);
                                               } };
    vk_graphics_context->device_queue = device_queue;
    vk_graphics_context->skia_context =
        decltype(VulkanGraphicsContext::skia_context){ skia_context.release(), [](GrContext *self) {
                                                          self->releaseResourcesAndAbandonContext();
                                                          self->unref();
                                                      } };

    native_context_ =
        decltype(native_context_){ vk_graphics_context, [](void *context) {
                                      auto *self = static_cast<VulkanGraphicsContext *>(context);
                                      delete self;
                                  } };
}

GraphicsContext::~GraphicsContext() noexcept = default;

VT_END_NAMESPACE

#endif
