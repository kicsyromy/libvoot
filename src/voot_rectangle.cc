#include "gui/voot_rectangle.hh"

#ifdef _MSC_VER
#pragma warning(disable : 4244 4201 4242 4996)
#endif
#include <core/SkCanvas.h>
#ifdef _MSC_VER
#pragma warning(default : 4244 4201 4242 4996)
#endif

VT_BEGIN_NAMESPACE

void Rectangle::render(SkCanvas *canvas) const noexcept
{
    canvas->save();

    const auto rectangle = SkRect::MakeXYWH(SkIntToScalar(x_abs()),
        SkIntToScalar(y_abs()),
        SkIntToScalar(width()),
        SkIntToScalar(height()));

    auto c = color();
    SkPaint paint(SkPaint(SkColor4f{ float(c.red) / 255,
        float(c.green) / 255,
        float(c.blue) / 255,
        float(c.alpha) / 255 }));

    canvas->drawRect(rectangle, paint);

    canvas->restore();
}

VT_END_NAMESPACE
