#ifdef VOOT_BACKEND_VULKAN

#include "core/private/voot_vulkan_helpers.hh"

#include "core/voot_logger.hh"

#include <SDL2.h>
#include <SDL_vulkan.h>

#ifdef _MSC_VER
#pragma warning(disable : 4244 4201 4242 4996)
#endif
#include <core/SkCanvas.h>
#include <core/SkSurface.h>
#include <gpu/GrContext.h>
#include <gpu/vk/GrVkBackendContext.h>
#include <gpu/vk/GrVkExtensions.h>
#ifdef _MSC_VER
#pragma warning(default : 4244 4201 4242 4996)
#endif

#include <algorithm>
#include <array>
#include <string>

VT_BEGIN_NAMESPACE

namespace
{
    namespace surface_detail
    {
        std::pair<VkSurfaceKHR, VkResult> create(SDL_Window *window,
            VkInstance instance,
            vulkan::PhysicalDevice physical_device)
        {
            VkSurfaceKHR surface{};
            auto res = SDL_Vulkan_CreateSurface(window, instance, &surface);
            if (res == SDL_FALSE)
            {
                VT_LOG_ERROR("Unable to create Vulkan compatible surface: {}", SDL_GetError());
                return { nullptr, VK_ERROR_UNKNOWN };
            }

            // Make sure the surface is compatible with the queue family and gpu
            VkBool32 supported = false;
            vkGetPhysicalDeviceSurfaceSupportKHR(physical_device.device,
                physical_device.queue_family_index,
                surface,
                &supported);
            if (!supported)
            {
                VT_LOG_ERROR("Surface is not supported by physical device!");
                return { nullptr, VK_ERROR_UNKNOWN };
            }

            return { surface, VK_SUCCESS };
        }

        std::pair<VkPresentModeKHR, VkResult> presentation_mode(VkSurfaceKHR surface,
            VkPhysicalDevice device,
            VkPresentModeKHR requested_mode)
        {
            std::uint32_t mode_count{ 0 };
            auto res =
                vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &mode_count, nullptr);
            if (res != VK_SUCCESS)
            {
                VT_LOG_ERROR("Unable to query present mode count for physical device");
                return { VK_PRESENT_MODE_MAX_ENUM_KHR, res };
            }

            std::vector<VkPresentModeKHR> available_modes;
            available_modes.resize(mode_count);
            res = vkGetPhysicalDeviceSurfacePresentModesKHR(device,
                surface,
                &mode_count,
                available_modes.data());
            if (res != VK_SUCCESS)
            {
                VT_LOG_ERROR("Unable to query the various present modes for physical device");
                return { VK_PRESENT_MODE_MAX_ENUM_KHR, res };
            }

            VkPresentModeKHR present_mode = VK_PRESENT_MODE_MAX_ENUM_KHR;
            for (auto &mode : available_modes)
            {
                if (mode == requested_mode)
                {
                    present_mode = mode;
                }
            }

            if (present_mode == VK_PRESENT_MODE_MAX_ENUM_KHR)
            {
                VT_LOG_WARN("Unable to obtain preferred display mode, fallback to FIFO");
            }

            return { present_mode, VK_SUCCESS };
        }

        std::pair<VkSurfaceCapabilitiesKHR, VkResult> capabilities(VkSurfaceKHR surface,
            VkPhysicalDevice device)
        {
            VkSurfaceCapabilitiesKHR capabilities{};
            auto res = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &capabilities);
            if (res != VK_SUCCESS)
            {
                VT_LOG_ERROR("Unable to acquire surface capabilities");
                return { {}, res };
            }

            return { capabilities, VK_SUCCESS };
        }

        std::pair<VkSurfaceFormatKHR, VkResult> format(VkSurfaceKHR surface,
            VkPhysicalDevice device,
            VkFormat requested_format,
            VkColorSpaceKHR requested_color_space)
        {
            VkSurfaceFormatKHR surface_format{ VK_FORMAT_MAX_ENUM, VK_COLOR_SPACE_MAX_ENUM_KHR };

            std::uint32_t count{ 0 };
            auto res = vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &count, nullptr);
            if (res != VK_SUCCESS)
            {
                VT_LOG_ERROR("Unable to query number of supported surface formats");
                return { surface_format, res };
            }

            std::vector<VkSurfaceFormatKHR> found_formats;
            found_formats.resize(count);
            res =
                vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &count, found_formats.data());
            if (res != VK_SUCCESS)
            {
                VT_LOG_ERROR("Unable to query all supported surface formats");
                return { surface_format, res };
            }

            // This means there are no restrictions on the supported format.
            // Preference would work
            if (found_formats.size() == 1 && found_formats[0].format == VK_FORMAT_UNDEFINED)
            {
                surface_format.format = requested_format;
                surface_format.colorSpace = requested_color_space;

                return { surface_format, VK_SUCCESS };
            }

            // Otherwise check if both are supported
            for (const auto &found_format_outer : found_formats)
            {
                // Format found
                if (found_format_outer.format == requested_format)
                {
                    surface_format.format = found_format_outer.format;
                    for (const auto &found_format_inner : found_formats)
                    {
                        // Color space found
                        if (found_format_inner.colorSpace == requested_color_space)
                        {
                            surface_format.colorSpace = found_format_inner.colorSpace;
                            return { surface_format, VK_SUCCESS };
                        }
                    }

                    VT_LOG_WARN("No matching color space found, picking first available one!");
                    surface_format.colorSpace = found_formats[0].colorSpace;

                    return { surface_format, VK_SUCCESS };
                }
            }

            VT_LOG_WARN("No matching color format found, picking first available one!");
            surface_format = found_formats[0];

            return { surface_format, VK_SUCCESS };
        }

        VkSurfaceTransformFlagBitsKHR transform_flags(const VkSurfaceCapabilitiesKHR &capabilities,
            VkSurfaceTransformFlagBitsKHR requested_flags)
        {
            if (capabilities.supportedTransforms & requested_flags)
            {
                return requested_flags;
            }

            VT_LOG_WARN("Unsupported surface transform: {}", requested_flags);

            return capabilities.currentTransform;
        }
    } // namespace surface_detail

    namespace swap_chain_detail
    {
        std::uint32_t image_count(const VkSurfaceCapabilitiesKHR &capabilities)
        {
            const auto number = capabilities.minImageCount + 1;
            return number > capabilities.maxImageCount ? capabilities.minImageCount : number;
        }

        VkExtent2D image_size(const VkSurfaceCapabilitiesKHR &capabilities,
            std::uint32_t windows_width,
            std::uint32_t window_height)
        {
            // Default size = window size
            auto size = VkExtent2D{ windows_width, window_height };

            // This happens when the window scales based on the size of an image
            if (capabilities.currentExtent.width == 0xFFFFFFF)
            {
                size.width = std::clamp(size.width,
                    capabilities.minImageExtent.width,
                    capabilities.maxImageExtent.width);
                size.height = std::clamp(size.height,
                    capabilities.maxImageExtent.height,
                    capabilities.maxImageExtent.height);
            }
            else
            {
                size = capabilities.currentExtent;
            }
            return size;
        }

        std::pair<VkImageUsageFlags, bool> image_usage_flags(
            const VkSurfaceCapabilitiesKHR &capabilities,
            const VkImageUsageFlags *requested_usage_flags,
            std::size_t usage_flags_count)
        {
            VkImageUsageFlags usage_flags = 0;

            for (std::size_t i = 0; i < usage_flags_count; ++i)
            {
                const auto &requested_usage = requested_usage_flags[i];
                VkImageUsageFlags image_usage = requested_usage & capabilities.supportedUsageFlags;
                if (image_usage != requested_usage)
                {
                    VT_LOG_ERROR("Unsupported image usage flag: {}", requested_usage);
                    return { VK_IMAGE_USAGE_FLAG_BITS_MAX_ENUM, false };
                }

                // Add bit if found as supported color
                usage_flags |= requested_usage;
            }

            return { usage_flags, true };
        }
    } // namespace swap_chain_detail
} // namespace

namespace vulkan
{
    std::vector<std::string> available_layer_cache;
    std::vector<std::string> available_extension_cache;

    std::pair<std::vector<const char *>, VkResult> available_layers()
    {
        std::vector<const char *> available_layers;

        if (!available_layer_cache.empty())
        {
            available_layers.reserve(available_layer_cache.size());

            VT_LOG_DEBUG("Found {} instance layers:", available_layer_cache.size());
            std::int32_t count{ 0 };
            for (const auto &layer : available_layer_cache)
            {
                available_layers.emplace_back(layer.c_str());
                VT_LOG_DEBUG("{}: {}", count, layer);
                ++count;
            }

            return { std::move(available_layers), VK_SUCCESS };
        }

        std::uint32_t instance_layer_count = 0;
        auto res = vkEnumerateInstanceLayerProperties(&instance_layer_count, nullptr);
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to query vulkan instance layer property count");
            return { available_layers, res };
        }

        std::vector<VkLayerProperties> instance_layers;
        instance_layers.resize(instance_layer_count);
        res = vkEnumerateInstanceLayerProperties(&instance_layer_count, instance_layers.data());
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to retrieve vulkan instance layer names");
            return { available_layers, res };
        }

        VT_LOG_DEBUG("Found {} instance layers:", instance_layer_count);

        available_extension_cache.reserve(instance_layers.size());
        available_layers.reserve(instance_layers.size());

        std::int32_t count{ 0 };
        for (const auto &layer : instance_layers)
        {
            available_layer_cache.emplace_back(layer.layerName);
            available_layers.emplace_back(available_layer_cache.back().c_str());
            VT_LOG_DEBUG("{}: {}", count, layer.layerName);
            ++count;
        }

        return { std::move(available_layers), VK_SUCCESS };
    }

    std::pair<std::vector<const char *>, VkResult> available_extensions()
    {
        std::vector<const char *> available_extensions;

        if (!available_extension_cache.empty())
        {
            available_extensions.reserve(available_extension_cache.size());

            VT_LOG_DEBUG("Found {} instance extensions:", available_extension_cache.size());
            std::int32_t count{ 0 };
            for (const auto &extension : available_extension_cache)
            {
                available_extensions.emplace_back(extension.c_str());
                VT_LOG_DEBUG("{}: {}", count, extension);
                ++count;
            }

            return { std::move(available_extensions), VK_SUCCESS };
        }

        std::uint32_t ext_count{ 0 };
        auto res = vkEnumerateInstanceExtensionProperties(nullptr, &ext_count, nullptr);
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to query the number of Vulkan instance extensions");
            return { available_extensions, res };
        }

        // Use the amount of extensions queried before to retrieve the names of the extensions
        std::vector<VkExtensionProperties> instance_extensions;
        instance_extensions.resize(ext_count);
        res =
            vkEnumerateInstanceExtensionProperties(nullptr, &ext_count, instance_extensions.data());
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to query Vulkan instance extension names");
            return { available_extensions, res };
        }

        VT_LOG_DEBUG("Found {} instance extensions:", ext_count);

        available_extensions.reserve(instance_extensions.size());
        available_extension_cache.reserve(instance_extensions.size());

        std::int32_t count{ 0 };
        for (const auto &extension : instance_extensions)
        {
            available_extension_cache.emplace_back(extension.extensionName);
            available_extensions.emplace_back(available_extension_cache.back().c_str());
            VT_LOG_DEBUG("{}: {}", count, extension.extensionName);
            ++count;
        }

        return { std::move(available_extensions), VK_SUCCESS };
    }

    std::pair<VkInstance, VkResult> create_instance(std::uint32_t api_version,
        const char *const *required_layers,
        std::size_t layer_count,
        const char *const *required_extensions,
        std::size_t extension_count)
    {
        // Get the supported vulkan instance version
        std::uint32_t version;
        vkEnumerateInstanceVersion(&version);

        // initialize the VkApplicationInfo structure
        VkApplicationInfo app_info = {};
        app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pNext = nullptr;
        app_info.pApplicationName = "libvoot";
        app_info.applicationVersion = 1;
        app_info.pEngineName = "skia";
        app_info.engineVersion = 1;
        app_info.apiVersion = api_version;

        // initialize the VkInstanceCreateInfo structure
        VkInstanceCreateInfo inst_info = {};
        inst_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        inst_info.pNext = nullptr;
        inst_info.flags = 0;
        inst_info.pApplicationInfo = &app_info;
        inst_info.enabledExtensionCount = std::uint32_t(extension_count);
        inst_info.ppEnabledExtensionNames = required_extensions;
        inst_info.enabledLayerCount = std::uint32_t(layer_count);
        inst_info.ppEnabledLayerNames = required_layers;

        VT_LOG_DEBUG("Initializing Vulkan instance");

        VkInstance vk_instance{};
        const auto res = vkCreateInstance(&inst_info, nullptr, &vk_instance);
        switch (res)
        {
        case VK_SUCCESS:
            return { vk_instance, res };
        case VK_ERROR_INCOMPATIBLE_DRIVER:
            VT_LOG_ERROR("Unable to create vulkan instance: Cannot find a compatible Vulkan ICD");
            return { nullptr, res };
        default:
            VT_LOG_ERROR("Unable to create vulkan instance: Unknown Error");
            return { nullptr, res };
        }
    }

    std::pair<std::vector<PhysicalDevice>, VkResult> device::available_physical_devices(
        VkInstance vk_instance,
        VkQueueFlags vk_queue_flag)
    {
        std::vector<PhysicalDevice> available_devices;

        // Get number of available physical devices, needs to be at least 1
        std::uint32_t physical_device_count{ 0 };
        auto res = vkEnumeratePhysicalDevices(vk_instance, &physical_device_count, nullptr);
        if (res != VK_SUCCESS || physical_device_count == 0)
        {
            VT_LOG_ERROR("No physical devices found, or available ones failed to enumerate");
            return { available_devices, res };
        }

        // Now get the devices
        std::vector<VkPhysicalDevice> physical_devices;
        physical_devices.resize(physical_device_count);
        res = vkEnumeratePhysicalDevices(vk_instance,
            &physical_device_count,
            physical_devices.data());
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Failed to retrieve device information");
            return { available_devices, res };
        }

        // Show device information
        VT_LOG_DEBUG("Found {} GPU(s):", physical_device_count);

        std::uint32_t count{ 0 };
        std::vector<VkPhysicalDeviceProperties> physical_device_properties(physical_devices.size());
        for (const auto &physical_device : physical_devices)
        {
            vkGetPhysicalDeviceProperties(physical_device, &(physical_device_properties[count]));
            VT_LOG_DEBUG("{}: {}", count, physical_device_properties[count].deviceName);
            ++count;
        }

        for (std::uint32_t selection_id = 0; selection_id < std::uint32_t(count); ++selection_id)
        {
            VkPhysicalDevice selected_device = physical_devices[selection_id];

            // Find the number queues this device supports, we want to make sure that we have a
            // queue that supports requested commands
            std::uint32_t family_queue_count{ 0 };
            vkGetPhysicalDeviceQueueFamilyProperties(selected_device, &family_queue_count, nullptr);
            if (family_queue_count == 0)
            {
                continue;
            }

            // Extract the properties of all the queue families
            std::vector<VkQueueFamilyProperties> queue_properties;
            queue_properties.resize(family_queue_count);
            vkGetPhysicalDeviceQueueFamilyProperties(selected_device,
                &family_queue_count,
                queue_properties.data());

            // Make sure the family of commands contains an option to issue the requested commands.
            std::uint32_t queue_family_index = std::numeric_limits<std::uint32_t>::max();
            for (unsigned int i = 0; i < family_queue_count; i++)
            {
                if (queue_properties[i].queueCount > 0 &&
                    queue_properties[i].queueFlags & vk_queue_flag)
                {
                    queue_family_index = i;
                    break;
                }
            }

            if (queue_family_index == std::numeric_limits<std::uint32_t>::max())
            {
                continue;
            }

            available_devices.emplace_back(PhysicalDevice{ selected_device, queue_family_index });
        }

        return { available_devices, VK_SUCCESS };
    }

    std::pair<Device, VkResult> device::create_logical_device(PhysicalDevice physical_device,
        const char *const *required_layers,
        std::size_t layers_count,
        const char *const *required_extensions,
        std::size_t extension_count)
    {
        Device logical_device{
            nullptr,
            0,
        };

        // Get the number of available extensions for our graphics card
        std::uint32_t device_property_count{ 0 };
        auto res = vkEnumerateDeviceExtensionProperties(physical_device.device,
            nullptr,
            &device_property_count,
            nullptr);
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to acquire device extension property count");
            return { logical_device, res };
        }

        VT_LOG_DEBUG("Found {} device extensions", device_property_count);

        // Acquire their actual names
        std::vector<VkExtensionProperties> device_extensions;
        device_extensions.resize(device_property_count);
        res = vkEnumerateDeviceExtensionProperties(physical_device.device,
            nullptr,
            &device_property_count,
            device_extensions.data());
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to acquire device extension property names");
            return { logical_device, res };
        }

        std::int32_t count{ 0 };
        for (const auto &extension : device_extensions)
        {
            VT_LOG_DEBUG("{}: {}", count, extension.extensionName);
            ++count;
        }

        // Create queue information structure used by device based on the previously fetched queue
        // information from the physical device We create one command processing queue for graphics
        VkDeviceQueueCreateInfo queue_create_info;
        queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queue_create_info.queueFamilyIndex = physical_device.queue_family_index;
        queue_create_info.queueCount = 1;
        std::array queue_priority{ 1.0F };
        queue_create_info.pQueuePriorities = queue_priority.data();
        queue_create_info.pNext = nullptr;
        queue_create_info.flags = 0;

        // Device creation information
        VkDeviceCreateInfo create_info;
        create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        create_info.queueCreateInfoCount = 1;
        create_info.pQueueCreateInfos = &queue_create_info;
        create_info.ppEnabledLayerNames = required_layers;
        create_info.enabledLayerCount = std::uint32_t(layers_count);
        create_info.ppEnabledExtensionNames = required_extensions;
        create_info.enabledExtensionCount = std::uint32_t(extension_count);
        create_info.pNext = nullptr;
        create_info.pEnabledFeatures = nullptr;
        create_info.flags = 0;

        // Finally we're ready to create a new device
        res = vkCreateDevice(physical_device.device, &create_info, nullptr, &logical_device.device);
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Failed to create logical device!");
            return { logical_device, res };
        }

        logical_device.queue_family_index = physical_device.queue_family_index;

        return { logical_device, VK_SUCCESS };
    }

    std::pair<VkSwapchainKHR, VkResult> swap_chain::create(SDL_Window *window,
        VkInstance instance,
        PhysicalDevice physical_device,
        Device logical_device,
        VkSurfaceTransformFlagBitsKHR surface_transform,
        VkFormat surface_format,
        VkColorSpaceKHR color_space,
        VkPresentModeKHR present_mode,
        const VkImageUsageFlags *image_usage_flags,
        std::size_t image_usage_flag_count,
        VkSwapchainKHR retired_swap_chain)

    {
        VkSwapchainKHR swap_chain{ nullptr };

        auto surface = surface_detail::create(window, instance, physical_device);
        if (surface.second != VK_SUCCESS)
        {
            return { nullptr, surface.second };
        }

        // Get properties of surface, necessary for creation of swap-chain
        const auto surface_capabilities =
            surface_detail::capabilities(surface.first, physical_device.device);
        if (surface_capabilities.second != VK_SUCCESS)
        {
            return { nullptr, surface_capabilities.second };
        }

        // Get the image presentation mode (synced, immediate etc.)
        const auto presentation_mode =
            surface_detail::presentation_mode(surface.first, physical_device.device, present_mode);
        if (presentation_mode.second != VK_SUCCESS)
        {
            return { nullptr, presentation_mode.second };
        }

        // Get other swap chain related features
        const auto swap_image_count = swap_chain_detail::image_count(surface_capabilities.first);

        // Size of the images
        int drawable_width{};
        int drawable_height{};
        SDL_Vulkan_GetDrawableSize(window, &drawable_width, &drawable_height);
        const auto swap_image_extent = swap_chain_detail::image_size(surface_capabilities.first,
            std::uint32_t(drawable_width),
            std::uint32_t(drawable_height));

        // Get image usage (color etc.)
        const auto usage_flags = swap_chain_detail::image_usage_flags(surface_capabilities.first,
            image_usage_flags,
            image_usage_flag_count);
        if (!usage_flags.second)
        {
            return { nullptr, VK_ERROR_FORMAT_NOT_SUPPORTED };
        }

        // Get the transform, falls back on current transform when transform is not supported
        VkSurfaceTransformFlagBitsKHR transform =
            surface_detail::transform_flags(surface_capabilities.first, surface_transform);

        // Get swap chain image format
        const auto image_format = surface_detail::format(surface.first,
            physical_device.device,
            surface_format,
            color_space);
        if (image_format.second != VK_SUCCESS)
        {
            return { nullptr, image_format.second };
        }

        // Populate swap chain creation info
        VkSwapchainCreateInfoKHR swap_info;
        swap_info.pNext = nullptr;
        swap_info.flags = 0;
        swap_info.surface = surface.first;
        swap_info.minImageCount = swap_image_count;
        swap_info.imageFormat = image_format.first.format;
        swap_info.imageColorSpace = image_format.first.colorSpace;
        swap_info.imageExtent = swap_image_extent;
        swap_info.imageArrayLayers = 1;
        swap_info.imageUsage = usage_flags.first;
        swap_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swap_info.queueFamilyIndexCount = 0;
        swap_info.pQueueFamilyIndices = nullptr;
        swap_info.preTransform = transform;
        swap_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        swap_info.presentMode = presentation_mode.first;
        swap_info.clipped = VK_TRUE;
        swap_info.oldSwapchain = retired_swap_chain;
        swap_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;

        // Create new one
        auto res = vkCreateSwapchainKHR(logical_device.device, &swap_info, nullptr, &swap_chain);
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to create swap chain");
            return { nullptr, res };
        }

        return { swap_chain, VK_SUCCESS };
    }

    std::pair<std::vector<VkImage>, VkResult> swap_chain::image_handles(VkSwapchainKHR swap_chain,
        VkDevice logical_device)
    {
        std::uint32_t image_count{ 0 };
        VkResult res = vkGetSwapchainImagesKHR(logical_device, swap_chain, &image_count, nullptr);
        if (res != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to obtain number of images in swap chain");
            return { {}, res };
        }

        std::vector<VkImage> image_handles{};
        image_handles.resize(image_count);
        if (vkGetSwapchainImagesKHR(logical_device,
                swap_chain,
                &image_count,
                image_handles.data()) != VK_SUCCESS)
        {
            VT_LOG_ERROR("Unable to obtain image handles from swap chain");
            return { {}, res };
        }

        return { image_handles, VK_SUCCESS };
    }

    VkQueue queue::create(Device device)
    {
        VkQueue queue{};
        vkGetDeviceQueue(device.device, device.queue_family_index, 0, &queue);

        return queue;
    }

    VkResult queue::present(VkQueue device_queue,
        const VkSwapchainKHR *swap_chains,
        std::size_t swap_chain_count,
        const std::uint32_t *image_indices)
    {
        VkPresentInfoKHR presentInfo{};
        presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        presentInfo.waitSemaphoreCount = 0;
        presentInfo.pWaitSemaphores = nullptr;
        presentInfo.swapchainCount = std::uint32_t(swap_chain_count);
        presentInfo.pSwapchains = swap_chains;
        presentInfo.pImageIndices = image_indices;

        return vkQueuePresentKHR(device_queue, &presentInfo);
    }
    std::unique_ptr<GrContext> skia::create_context(VkInstance instance,
        std::uint32_t instance_api_version,
        PhysicalDevice physical_device,
        Device logical_device,
        VkQueue device_queue,
        const char *const *instance_extensions,
        std::size_t instance_extension_count,
        const char *const *device_extensions,
        std::size_t device_extension_count)
    {

        GrVkBackendContext vkContext;
        vkContext.fInstance = instance;
        vkContext.fPhysicalDevice = physical_device.device;
        vkContext.fDevice = logical_device.device;
        vkContext.fQueue = device_queue;
        vkContext.fGraphicsQueueIndex = physical_device.queue_family_index;
        vkContext.fMaxAPIVersion = instance_api_version;
        vkContext.fGetProc = [](const char *proc_name, VkInstance instance, VkDevice device) {
            return device != VK_NULL_HANDLE ? vkGetDeviceProcAddr(device, proc_name)
                                            : vkGetInstanceProcAddr(instance, proc_name);
        };

        GrVkExtensions gr_vk_extensions;
        gr_vk_extensions.init(vkContext.fGetProc,
            instance,
            physical_device.device,
            std::uint32_t(instance_extension_count),
            instance_extensions,
            std::uint32_t(device_extension_count),
            device_extensions);
        vkContext.fVkExtensions = &gr_vk_extensions;

        auto skia_context = GrContext::MakeVulkan(vkContext);

        return std::unique_ptr<GrContext>{ skia_context.release() };
    }
    std::pair<std::unique_ptr<SkSurface>, std::unique_ptr<GrBackendRenderTarget>> skia::
        create_surface(SDL_Window *window,
            GrContext &skia_context,
            VkImage image_handle,
            VkFormat swap_chain_format,
            PhysicalDevice physical_device)
    {
        GrVkImageInfo gr_vk_image_info{ image_handle,
            GrVkAlloc{},
            VK_IMAGE_TILING_OPTIMAL,
            VK_IMAGE_LAYOUT_GENERAL,
            swap_chain_format,
            1,
            physical_device.queue_family_index };

        int drawable_width{};
        int drawable_height{};
        SDL_Vulkan_GetDrawableSize(window, &drawable_width, &drawable_height);
        auto *render_target =
            new GrBackendRenderTarget(drawable_width, drawable_height, 0, gr_vk_image_info);
        SkSurfaceProps surface_properties(SkSurfaceProps::kLegacyFontHost_InitType);
        sk_sp<SkSurface> skia_surface(SkSurface::MakeFromBackendRenderTarget(&skia_context,
            *render_target,
            kTopLeft_GrSurfaceOrigin,
            kN32_SkColorType,
            nullptr,
            &surface_properties));

        return { std::unique_ptr<SkSurface>{ skia_surface.release() },
            std::unique_ptr<GrBackendRenderTarget>{ render_target } };
    }
} // namespace vulkan

VT_END_NAMESPACE

#endif