#include "gui/voot_window.hh"
#include "voot_global.hh"

#include <SDL2.h>
#include <SDL_syswm.h>

#ifdef _MSC_VER
#pragma warning(disable : 4244 4201 4242 4996)
#endif
#include <core/SkCanvas.h>
#include <core/SkSurface.h>
#ifdef _MSC_VER
#pragma warning(default : 4244 4201 4242 4996)
#endif

namespace
{
    constexpr auto w_width{ 800 };
    constexpr auto w_height{ 800 };
} // namespace

VT_BEGIN_NAMESPACE

Window::Window(std::string_view title) noexcept
  : width_{ w_width }
  , height_{ w_height }
  , window_handle_{ SDL_CreateWindow(title.data(),
                        SDL_WINDOWPOS_UNDEFINED,
                        SDL_WINDOWPOS_UNDEFINED,
                        int(width_),
                        int(height_),
                        SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
#ifdef VOOT_BACKEND_GLES
                            | SDL_WINDOW_OPENGL
#elif VOOT_BACKEND_VULKAN
                            | SDL_WINDOW_VULKAN
#endif
                        ),
      &SDL_DestroyWindow }
  , surface_{ VT_APPLICATION()->graphics_context(), window_handle_.get() }
  , root_item_{}
{
    if (window_handle_ == nullptr)
    {
        VT_LOG_FATAL("Failed to create window: {}", SDL_GetError());
    }

    root_item_.focus = true;
    root_item_.width = width_;
    root_item_.height = height_;

    auto *app = VT_APPLICATION();
    assert(app != nullptr);

    app->register_event_handler<KeyPressEvent, &Window::on_key_press_event>(this);

    app->register_event_handler<KeyReleaseEvent, &Window::on_key_release_event>(this);

    app->register_event_handler<MouseMoveEvent, &Window::on_mouse_move_event>(this);

    app->register_event_handler<MouseButtonPressEvent, &Window::on_mouse_button_press_event>(this);

    app->register_event_handler<MouseButtonReleaseEvent, &Window::on_mouse_button_release_event>(
        this);

    app->register_event_handler<WindowResizeEvent, &Window::on_window_resized_event>(this);

    app->register_event_handler<WindowCloseEvent, &Window::on_window_closed_event>(this);

    app->register_event_handler<RenderEvent, &Window::on_render_event>(this);
}

Window::~Window() noexcept = default;

std::pair<std::size_t, std::size_t> voot::Window::viewport_size() const noexcept
{
    return { 0, 0 };
}

void *Window::native_window_handle() const noexcept
{
    SDL_SysWMinfo wmi;
    SDL_VERSION(&wmi.version);
    if (!SDL_GetWindowWMInfo(window_handle_.get(), &wmi))
    {
        VT_LOG_ERROR("Failed to get native window handle: {}", SDL_GetError());

        return nullptr;
    }

#ifdef _WIN32
    return reinterpret_cast<void *>(wmi.info.win.window);
#elif __linux__
    return reinterpret_cast<void *>(wmi.info.x11.window);
#else
    return reinterpret_cast<void *>(wmi.info.cocoa.window);
#endif
}

bool Window::on_key_press_event(int window_id, KeyPressEvent *event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    if (event->key() == KeyCode::Tab)
    {
        root_item_.move_next();
    }

    return true;
}

bool Window::on_key_release_event(int window_id, KeyReleaseEvent *event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    VT_LOG_DEBUG("[window] key release");

    return true;
}

bool Window::on_mouse_move_event(int window_id, MouseMoveEvent *event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    VT_LOG_DEBUG("[window] mouse move");

    return true;
}

bool Window::on_mouse_button_press_event(int window_id, MouseButtonPressEvent *event) noexcept
{
    static_cast<void>(window_id);

    VT_LOG_DEBUG("[window] mouse button press B: {} X: {} Y: {}",
        event->button(),
        event->coordinates().first,
        event->coordinates().second);
    const auto [x, y] = event->coordinates();
    root_item_.handle_mouse_button_pressed(event->button(), x, y);

    return true;
}

bool Window::on_mouse_button_release_event(int window_id, MouseButtonReleaseEvent *event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    VT_LOG_DEBUG("[window] mouse button release");
    const auto [x, y] = event->coordinates();
    root_item_.handle_mouse_button_released(event->button(), x, y);

    return true;
}

bool Window::on_window_resized_event(int window_id, WindowResizeEvent *event) noexcept
{
    static_cast<void>(window_id);

    auto [new_width, new_height] = event->size();
    root_item_.width = new_width;
    root_item_.height = new_height;

    surface_.refresh();

    return true;
}

bool Window::on_window_closed_event(int window_id, WindowCloseEvent *event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    VT_LOG_DEBUG("[window] window closed");

    return true;
}

bool Window::on_render_event(int window_id, RenderEvent *event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    surface_.set_as_current();

    auto &skia_surface = surface_.surface_handle();

    auto *canvas = skia_surface.getCanvas();
    root_item_.render(canvas);

    skia_surface.flushAndSubmit();
    surface_.present();

    return true;
}

VT_END_NAMESPACE
